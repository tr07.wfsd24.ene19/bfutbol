export const translate = (str) => {

    let newStr

    switch (str) {
        case 'GOAL':
            newStr = 'Gol'
            break;
        case 'GOAL_PENALTY':
            newStr = 'Gol de penalti'
            break;
        case 'YELLOW_CARD':
            newStr = 'Tarjeta amarilla'
            break;
        case 'RED_CARD':
            newStr = 'Tarjeta roja'
            break;
        case 'OWN_GOAL':
            newStr = 'Gol P.P'
            break;
        case 'YELLOW_RED_CARD':
            newStr = '2ª t.amarilla'
            break;
        default:
            break;
    }

    return newStr
}

export const translate2 = (str) => {

    let newStr

    switch (str) {
        case 'NOT STARTED':
            newStr = 'No ha comenzado'
            break;
        case 'IN PLAY':
            newStr = 'En juego'
            break;
        case 'HALF TIME BREAK':
            newStr = 'Descanso'
            break;
        case 'ADDED TIME':
            newStr = 'Tiempo añadido'
            break;
        case 'FINISHED':
            newStr = 'Finalizado'
            break;
        case 'INSUFFICIENT DATA':
            newStr = 'Insuficientes datos'
            break;
        default:
            break;
    }

    return newStr
}

