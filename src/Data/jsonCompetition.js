// 20190817153523
// https://livescore-api.com/api-client/competitions/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country_id=0
import countryJson from "./jsonCountry";

const competitionJson = {
    "dataC": {
    "competition": [
        {
            "id": "339",
            "name": "1. Liga Promotion",
            "countries": [
                {
                    "id": "9",
                    "name": "Switzerland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "125",
            "name": "1st Deild",
            "countries": [
                {
                    "id": "33",
                    "name": "Iceland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "309",
            "name": "1st Division",
            "countries": [
                {
                    "id": "12",
                    "name": "Russia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "327",
            "name": "1st Division",
            "countries": [
                {
                    "id": "69",
                    "name": "South Africa"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "353",
            "name": "1st Division",
            "countries": [
                {
                    "id": "13",
                    "name": "Ukraine"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "132",
            "name": "1st Division",
            "countries": [
                {
                    "id": "102",
                    "name": "Albania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "147",
            "name": "1st Division",
            "countries": [
                {
                    "id": "5",
                    "name": "Denmark"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "191",
            "name": "1st Division",
            "countries": [
                {
                    "id": "120",
                    "name": "Malta"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "216",
            "name": "1st Division",
            "countries": [
                {
                    "id": "23",
                    "name": "Ireland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "17",
            "name": "1st League",
            "countries": [
                {
                    "id": "54",
                    "name": "Croatia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "44",
            "name": "1st League",
            "countries": [
                {
                    "id": "49",
                    "name": "Cyprus"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "72",
            "name": "1st League",
            "countries": [
                {
                    "id": "11",
                    "name": "Czech Republic"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "138",
            "name": "1st League",
            "countries": [
                {
                    "id": "39",
                    "name": "Bosnia and Herzegovina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "187",
            "name": "1st League",
            "countries": [
                {
                    "id": "68",
                    "name": "Lithuania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "239",
            "name": "1st League",
            "countries": [
                {
                    "id": "190",
                    "name": "Armenia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "344",
            "name": "1st Lig",
            "countries": [
                {
                    "id": "48",
                    "name": "Turkey"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "209",
            "name": "1st Liga",
            "countries": [
                {
                    "id": "14",
                    "name": "Poland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "93",
            "name": "2nd Bundesliga",
            "countries": [
                {
                    "id": "1",
                    "name": "Germany"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "306",
            "name": "2nd Division",
            "countries": [
                {
                    "id": "112",
                    "name": "Qatar"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "310",
            "name": "2nd Division",
            "countries": [
                {
                    "id": "12",
                    "name": "Russia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "148",
            "name": "2nd Division",
            "countries": [
                {
                    "id": "5",
                    "name": "Denmark"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "207",
            "name": "2nd Division",
            "countries": [
                {
                    "id": "6",
                    "name": "Norway"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "324",
            "name": "2nd League",
            "countries": [
                {
                    "id": "53",
                    "name": "Slovakia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "141",
            "name": "2nd League",
            "countries": [
                {
                    "id": "54",
                    "name": "Croatia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "144",
            "name": "2nd League",
            "countries": [
                {
                    "id": "49",
                    "name": "Cyprus"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "145",
            "name": "2nd league",
            "countries": [
                {
                    "id": "11",
                    "name": "Czech Republic"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "195",
            "name": "2nd League",
            "countries": [
                {
                    "id": "40",
                    "name": "Montenegro"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "345",
            "name": "2nd Lig",
            "countries": [
                {
                    "id": "48",
                    "name": "Turkey"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "210",
            "name": "2nd Liga",
            "countries": [
                {
                    "id": "14",
                    "name": "Poland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "325",
            "name": "2nd SNL",
            "countries": [
                {
                    "id": "34",
                    "name": "Slovenia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "146",
            "name": "3rd league",
            "countries": [
                {
                    "id": "11",
                    "name": "Czech Republic"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "346",
            "name": "3rd Lig",
            "countries": [
                {
                    "id": "48",
                    "name": "Turkey"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "166",
            "name": "3rd Liga",
            "countries": [
                {
                    "id": "1",
                    "name": "Germany"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "58",
            "name": "A Lyga",
            "countries": [
                {
                    "id": "68",
                    "name": "Lithuania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "242",
            "name": "AFC Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "246",
            "name": "AFF Suzuki Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "227",
            "name": "Africa Cup of Nations",
            "countries": [

            ],
            "federations": [
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "228",
            "name": "Africa Cup of Nations Qualifications",
            "countries": [

            ],
            "federations": [
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "226",
            "name": "African Nations Championship",
            "countries": [

            ],
            "federations": [
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "14",
            "name": "Allsvenskan",
            "countries": [
                {
                    "id": "7",
                    "name": "Sweden"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "366",
            "name": "Arab Club Champions Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "98",
            "name": "Ascenso MX",
            "countries": [
                {
                    "id": "57",
                    "name": "Mexico"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "240",
            "name": "Asian Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "241",
            "name": "Asian Cup Qualification",
            "countries": [

            ],
            "federations": [
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "367",
            "name": "Atlantic Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "368",
            "name": "Audi Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "173",
            "name": "B Ethniki",
            "countries": [
                {
                    "id": "78",
                    "name": "Greece"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "99",
            "name": "Beker Van Belgie",
            "countries": [
                {
                    "id": "2",
                    "name": "Belgium"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "134",
            "name": "Birinci Dasta",
            "countries": [
                {
                    "id": "28",
                    "name": "Azerbaijan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "38",
            "name": "Botola Pro",
            "countries": [
                {
                    "id": "81",
                    "name": "Morocco"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "1",
            "name": "Bundesliga",
            "countries": [
                {
                    "id": "1",
                    "name": "Germany"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "43",
            "name": "Bundesliga",
            "countries": [
                {
                    "id": "8",
                    "name": "Austria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "222",
            "name": "CAF Super Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "214",
            "name": "Campeonato de Portugal",
            "countries": [
                {
                    "id": "32",
                    "name": "Portugal"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "257",
            "name": "Canadian Championship",
            "countries": [
                {
                    "id": "119",
                    "name": "Canada"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "297",
            "name": "Central Premier League",
            "countries": [
                {
                    "id": "114",
                    "name": "New Zealand"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "316",
            "name": "Challenge Cup",
            "countries": [
                {
                    "id": "3",
                    "name": "Scotland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "338",
            "name": "Challenge League",
            "countries": [
                {
                    "id": "9",
                    "name": "Switzerland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "89",
            "name": "Championnat National",
            "countries": [
                {
                    "id": "97",
                    "name": "Rwanda"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "369",
            "name": "Champions Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "268",
            "name": "Champions League",
            "countries": [

            ],
            "federations": [
                {
                    "id": "5",
                    "name": "CONCACAF"
                }
            ]
        },
        {
            "id": "300",
            "name": "Champions League",
            "countries": [

            ],
            "federations": [
                {
                    "id": "7",
                    "name": "OFC"
                }
            ]
        },
        {
            "id": "223",
            "name": "Champions League",
            "countries": [

            ],
            "federations": [
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "243",
            "name": "Champions League",
            "countries": [

            ],
            "federations": [
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "244",
            "name": "Champions League",
            "countries": [

            ],
            "federations": [
                {
                    "id": "2",
                    "name": "UEFA"
                }
            ]
        },
        {
            "id": "317",
            "name": "Championship",
            "countries": [
                {
                    "id": "3",
                    "name": "Scotland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "77",
            "name": "Championship",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "370",
            "name": "Club Friendlies",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "149",
            "name": "Community Shield",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                },
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "267",
            "name": "CONCACAF League",
            "countries": [

            ],
            "federations": [
                {
                    "id": "5",
                    "name": "CONCACAF"
                }
            ]
        },
        {
            "id": "269",
            "name": "CONCACAF Nations League Qualification",
            "countries": [

            ],
            "federations": [
                {
                    "id": "5",
                    "name": "CONCACAF"
                }
            ]
        },
        {
            "id": "224",
            "name": "Confederations Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "271",
            "name": "Copa America",
            "countries": [

            ],
            "federations": [
                {
                    "id": "6",
                    "name": "CONMEBOL"
                }
            ]
        },
        {
            "id": "230",
            "name": "Copa Argentina",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "116",
            "name": "Copa Colombia",
            "countries": [
                {
                    "id": "61",
                    "name": "Colombia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "231",
            "name": "Copa de la Superliga",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "334",
            "name": "Copa del Rey",
            "countries": [
                {
                    "id": "43",
                    "name": "Spain"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "256",
            "name": "Copa Do Brasil",
            "countries": [
                {
                    "id": "16",
                    "name": "Brazil"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "329",
            "name": "Copa Libertadores",
            "countries": [

            ],
            "federations": [
                {
                    "id": "6",
                    "name": "CONMEBOL"
                }
            ]
        },
        {
            "id": "117",
            "name": "Copa MX",
            "countries": [
                {
                    "id": "57",
                    "name": "Mexico"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "330",
            "name": "Copa Sudamericana",
            "countries": [

            ],
            "federations": [
                {
                    "id": "6",
                    "name": "CONMEBOL"
                }
            ]
        },
        {
            "id": "179",
            "name": "Coppa Italia",
            "countries": [
                {
                    "id": "47",
                    "name": "Italy"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "180",
            "name": "Coppa Italia Serie C",
            "countries": [
                {
                    "id": "47",
                    "name": "Italy"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "225",
            "name": "COSAFA Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "260",
            "name": "Cup",
            "countries": [
                {
                    "id": "41",
                    "name": "Chile"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "262",
            "name": "Cup",
            "countries": [
                {
                    "id": "26",
                    "name": "China"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "272",
            "name": "Cup",
            "countries": [
                {
                    "id": "74",
                    "name": "Egypt"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "275",
            "name": "Cup",
            "countries": [
                {
                    "id": "105",
                    "name": "Fiji"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "276",
            "name": "Cup",
            "countries": [
                {
                    "id": "17",
                    "name": "Georgia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "285",
            "name": "Cup",
            "countries": [
                {
                    "id": "72",
                    "name": "Japan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "295",
            "name": "Cup",
            "countries": [
                {
                    "id": "96",
                    "name": "Malaysia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "311",
            "name": "Cup",
            "countries": [
                {
                    "id": "12",
                    "name": "Russia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "315",
            "name": "Cup",
            "countries": [
                {
                    "id": "75",
                    "name": "Saudi Arabia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "323",
            "name": "Cup",
            "countries": [
                {
                    "id": "73",
                    "name": "Singapore"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "326",
            "name": "Cup",
            "countries": [
                {
                    "id": "34",
                    "name": "Slovenia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "335",
            "name": "Cup",
            "countries": [
                {
                    "id": "7",
                    "name": "Sweden"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "340",
            "name": "Cup",
            "countries": [
                {
                    "id": "9",
                    "name": "Switzerland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "342",
            "name": "Cup",
            "countries": [
                {
                    "id": "27",
                    "name": "Thailand"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "347",
            "name": "Cup",
            "countries": [
                {
                    "id": "48",
                    "name": "Turkey"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "351",
            "name": "Cup",
            "countries": [
                {
                    "id": "98",
                    "name": "Uganda"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "356",
            "name": "Cup",
            "countries": [
                {
                    "id": "76",
                    "name": "United Arab Emirates"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "106",
            "name": "Cup",
            "countries": [
                {
                    "id": "39",
                    "name": "Bosnia and Herzegovina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "107",
            "name": "Cup",
            "countries": [
                {
                    "id": "38",
                    "name": "Bulgaria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "108",
            "name": "Cup",
            "countries": [
                {
                    "id": "11",
                    "name": "Czech Republic"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "109",
            "name": "Cup",
            "countries": [
                {
                    "id": "55",
                    "name": "Israel"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "110",
            "name": "Cup",
            "countries": [
                {
                    "id": "68",
                    "name": "Lithuania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "111",
            "name": "Cup",
            "countries": [
                {
                    "id": "14",
                    "name": "Poland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "112",
            "name": "Cup",
            "countries": [
                {
                    "id": "36",
                    "name": "Romania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "113",
            "name": "Cup",
            "countries": [
                {
                    "id": "50",
                    "name": "Serbia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "114",
            "name": "Cup",
            "countries": [
                {
                    "id": "53",
                    "name": "Slovakia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "115",
            "name": "Cup",
            "countries": [
                {
                    "id": "13",
                    "name": "Ukraine"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "119",
            "name": "Cup",
            "countries": [
                {
                    "id": "63",
                    "name": "El Salvador"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "120",
            "name": "Cup",
            "countries": [
                {
                    "id": "59",
                    "name": "Guatemala"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "379",
            "name": "Cup",
            "countries": [
                {
                    "id": "45",
                    "name": "Venezuela"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "126",
            "name": "Cup",
            "countries": [
                {
                    "id": "33",
                    "name": "Iceland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "128",
            "name": "Cup",
            "countries": [
                {
                    "id": "15",
                    "name": "Belarus"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "131",
            "name": "Cup",
            "countries": [
                {
                    "id": "102",
                    "name": "Albania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "388",
            "name": "Cup",
            "countries": [
                {
                    "id": "62",
                    "name": "Paraguay"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "135",
            "name": "Cup",
            "countries": [
                {
                    "id": "28",
                    "name": "Azerbaijan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "142",
            "name": "Cup",
            "countries": [
                {
                    "id": "54",
                    "name": "Croatia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "157",
            "name": "Cup",
            "countries": [
                {
                    "id": "25",
                    "name": "Estonia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "159",
            "name": "Cup",
            "countries": [
                {
                    "id": "22",
                    "name": "Finland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "162",
            "name": "Cup",
            "countries": [
                {
                    "id": "21",
                    "name": "France"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "171",
            "name": "Cup",
            "countries": [
                {
                    "id": "78",
                    "name": "Greece"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "175",
            "name": "Cup",
            "countries": [
                {
                    "id": "35",
                    "name": "Hungary"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "185",
            "name": "Cup",
            "countries": [
                {
                    "id": "24",
                    "name": "Latvia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "188",
            "name": "Cup",
            "countries": [
                {
                    "id": "10",
                    "name": "Luxembourg"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "190",
            "name": "Cup",
            "countries": [
                {
                    "id": "120",
                    "name": "Malta"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "193",
            "name": "Cup",
            "countries": [
                {
                    "id": "37",
                    "name": "Moldova"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "194",
            "name": "Cup",
            "countries": [
                {
                    "id": "40",
                    "name": "Montenegro"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "202",
            "name": "Cup",
            "countries": [
                {
                    "id": "51",
                    "name": "North Macedonia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "206",
            "name": "Cup",
            "countries": [
                {
                    "id": "6",
                    "name": "Norway"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "212",
            "name": "Cup",
            "countries": [
                {
                    "id": "32",
                    "name": "Portugal"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "101",
            "name": "Cypriot Cup ",
            "countries": [
                {
                    "id": "49",
                    "name": "Cyprus"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "102",
            "name": "DBU Pokalen",
            "countries": [
                {
                    "id": "5",
                    "name": "Denmark"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "167",
            "name": "DFB Cup",
            "countries": [
                {
                    "id": "1",
                    "name": "Germany"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "288",
            "name": "Division 1",
            "countries": [
                {
                    "id": "89",
                    "name": "Jordan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "314",
            "name": "Division 1",
            "countries": [
                {
                    "id": "75",
                    "name": "Saudi Arabia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "337",
            "name": "Division 1",
            "countries": [
                {
                    "id": "7",
                    "name": "Sweden"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "355",
            "name": "Division 1",
            "countries": [
                {
                    "id": "76",
                    "name": "United Arab Emirates"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "49",
            "name": "Division Profesional",
            "countries": [
                {
                    "id": "62",
                    "name": "Paraguay"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "199",
            "name": "Eerste Divisie",
            "countries": [
                {
                    "id": "42",
                    "name": "Netherlands"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "150",
            "name": "EFL Cup",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                },
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "151",
            "name": "EFL Trophy",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                },
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "60",
            "name": "Ekstraklasa",
            "countries": [
                {
                    "id": "14",
                    "name": "Poland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "13",
            "name": "Eliteserien",
            "countries": [
                {
                    "id": "6",
                    "name": "Norway"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "196",
            "name": "Eredivisie",
            "countries": [
                {
                    "id": "42",
                    "name": "Netherlands"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "32",
            "name": "Erovnuli Liga",
            "countries": [
                {
                    "id": "17",
                    "name": "Georgia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "165",
            "name": "Erovnuli Liga 2",
            "countries": [
                {
                    "id": "17",
                    "name": "Georgia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "133",
            "name": "Erste Liga",
            "countries": [
                {
                    "id": "8",
                    "name": "Austria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "245",
            "name": "Europa League",
            "countries": [

            ],
            "federations": [
                {
                    "id": "2",
                    "name": "UEFA"
                }
            ]
        },
        {
            "id": "152",
            "name": "FA Cup",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                },
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "153",
            "name": "FA Trophy",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                },
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "217",
            "name": "FAI Cup",
            "countries": [
                {
                    "id": "23",
                    "name": "Ireland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "122",
            "name": "FFA Cup",
            "countries": [
                {
                    "id": "71",
                    "name": "Australia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "372",
            "name": "FIFA Club World Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "270",
            "name": "FIFA Confederations Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "362",
            "name": "FIFA World Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "68",
            "name": "First Division A",
            "countries": [
                {
                    "id": "2",
                    "name": "Belgium"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "136",
            "name": "First Division B",
            "countries": [
                {
                    "id": "2",
                    "name": "Belgium"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "71",
            "name": "First Professional League",
            "countries": [
                {
                    "id": "38",
                    "name": "Bulgaria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "63",
            "name": "Fortuna League",
            "countries": [
                {
                    "id": "53",
                    "name": "Slovakia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "371",
            "name": "Friendlies",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "172",
            "name": "Gamma Ethniki",
            "countries": [
                {
                    "id": "78",
                    "name": "Greece"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "27",
            "name": "GO-JEK Liga 1",
            "countries": [
                {
                    "id": "90",
                    "name": "Indonesia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "266",
            "name": "Gold Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "5",
                    "name": "CONCACAF"
                }
            ]
        },
        {
            "id": "291",
            "name": "GOTV Shield Cup",
            "countries": [
                {
                    "id": "29",
                    "name": "Kenya"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "280",
            "name": "Hazfi Cup",
            "countries": [
                {
                    "id": "18",
                    "name": "Iran"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "67",
            "name": "Hyundai A-League",
            "countries": [
                {
                    "id": "71",
                    "name": "Australia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "279",
            "name": "I-League",
            "countries": [
                {
                    "id": "87",
                    "name": "India"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "65",
            "name": "Indian Super League",
            "countries": [
                {
                    "id": "87",
                    "name": "India"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "103",
            "name": "Irish Cup",
            "countries": [
                {
                    "id": "79",
                    "name": "Northern Ireland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "28",
            "name": "J. League",
            "countries": [
                {
                    "id": "72",
                    "name": "Japan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "283",
            "name": "J. League 2",
            "countries": [
                {
                    "id": "72",
                    "name": "Japan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "66",
            "name": "K-League 1",
            "countries": [
                {
                    "id": "70",
                    "name": "Republic of Korea"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "308",
            "name": "K-League 2",
            "countries": [
                {
                    "id": "70",
                    "name": "Republic of Korea"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "386",
            "name": "Kagame Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "164",
            "name": "Kakkonen",
            "countries": [
                {
                    "id": "22",
                    "name": "Finland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "130",
            "name": "Kategoria Superiore",
            "countries": [
                {
                    "id": "102",
                    "name": "Albania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "373",
            "name": "King's Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "374",
            "name": "Kirin Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "198",
            "name": "KNVB Beker",
            "countries": [
                {
                    "id": "42",
                    "name": "Netherlands"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "121",
            "name": "Korean Cup",
            "countries": [
                {
                    "id": "70",
                    "name": "Republic of Korea"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "3",
            "name": "LaLiga Santander",
            "countries": [
                {
                    "id": "43",
                    "name": "Spain"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "263",
            "name": "League 1",
            "countries": [
                {
                    "id": "26",
                    "name": "China"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "318",
            "name": "League 1",
            "countries": [
                {
                    "id": "3",
                    "name": "Scotland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "82",
            "name": "League 1",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "319",
            "name": "League 2",
            "countries": [
                {
                    "id": "3",
                    "name": "Scotland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "83",
            "name": "League 2",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "284",
            "name": "League Cup",
            "countries": [
                {
                    "id": "72",
                    "name": "Japan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "320",
            "name": "League Cup",
            "countries": [
                {
                    "id": "3",
                    "name": "Scotland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "322",
            "name": "League Cup",
            "countries": [
                {
                    "id": "73",
                    "name": "Singapore"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "357",
            "name": "League Cup",
            "countries": [
                {
                    "id": "76",
                    "name": "United Arab Emirates"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "124",
            "name": "League Cup",
            "countries": [
                {
                    "id": "69",
                    "name": "South Africa"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "381",
            "name": "League Cup",
            "countries": [
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "163",
            "name": "League Cup",
            "countries": [
                {
                    "id": "21",
                    "name": "France"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "203",
            "name": "League Cup",
            "countries": [
                {
                    "id": "79",
                    "name": "Northern Ireland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "213",
            "name": "League Cup",
            "countries": [
                {
                    "id": "32",
                    "name": "Portugal"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "218",
            "name": "League Cup",
            "countries": [
                {
                    "id": "23",
                    "name": "Ireland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "221",
            "name": "League Cup",
            "countries": [
                {
                    "id": "36",
                    "name": "Romania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "177",
            "name": "Leumit League",
            "countries": [
                {
                    "id": "55",
                    "name": "Israel"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "61",
            "name": "Liga I",
            "countries": [
                {
                    "id": "36",
                    "name": "Romania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "220",
            "name": "Liga II",
            "countries": [
                {
                    "id": "36",
                    "name": "Romania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "45",
            "name": "Liga MX",
            "countries": [
                {
                    "id": "57",
                    "name": "Mexico"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "55",
            "name": "Liga Nacional",
            "countries": [
                {
                    "id": "59",
                    "name": "Guatemala"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "56",
            "name": "Liga Nacional",
            "countries": [
                {
                    "id": "64",
                    "name": "Honduras"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "73",
            "name": "Ligat HaAl",
            "countries": [
                {
                    "id": "55",
                    "name": "Israel"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "5",
            "name": "Ligue 1",
            "countries": [
                {
                    "id": "21",
                    "name": "France"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "35",
            "name": "Ligue 1",
            "countries": [
                {
                    "id": "67",
                    "name": "Algeria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "84",
            "name": "Ligue 1",
            "countries": [
                {
                    "id": "103",
                    "name": "Senegal"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "343",
            "name": "Ligue 2",
            "countries": [
                {
                    "id": "80",
                    "name": "Tunisia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "97",
            "name": "Ligue 2",
            "countries": [
                {
                    "id": "21",
                    "name": "France"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "229",
            "name": "Ligue 2",
            "countries": [
                {
                    "id": "67",
                    "name": "Algeria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "42",
            "name": "Ligue I",
            "countries": [
                {
                    "id": "80",
                    "name": "Tunisia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "304",
            "name": "LPF",
            "countries": [
                {
                    "id": "95",
                    "name": "Panama"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "298",
            "name": "Mainland Premier League",
            "countries": [
                {
                    "id": "114",
                    "name": "New Zealand"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "76",
            "name": "Major League Soccer",
            "countries": [
                {
                    "id": "46",
                    "name": "USA"
                },
                {
                    "id": "119",
                    "name": "Canada"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "382",
            "name": "NASL",
            "countries": [
                {
                    "id": "46",
                    "name": "USA"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "161",
            "name": "National 1",
            "countries": [
                {
                    "id": "21",
                    "name": "France"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "12",
            "name": "National Division",
            "countries": [
                {
                    "id": "10",
                    "name": "Luxembourg"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "59",
            "name": "National Division",
            "countries": [
                {
                    "id": "37",
                    "name": "Moldova"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "154",
            "name": "National League",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                },
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "155",
            "name": "National League North / South",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                },
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "249",
            "name": "National Premier leagues",
            "countries": [
                {
                    "id": "71",
                    "name": "Australia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "174",
            "name": "NB II",
            "countries": [
                {
                    "id": "35",
                    "name": "Hungary"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "250",
            "name": "New South Wales",
            "countries": [
                {
                    "id": "71",
                    "name": "Australia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "78",
            "name": "NPFL",
            "countries": [
                {
                    "id": "108",
                    "name": "Nigeria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "204",
            "name": "OBOS-ligaen",
            "countries": [
                {
                    "id": "6",
                    "name": "Norway"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "100",
            "name": "ÖFB Cup",
            "countries": [
                {
                    "id": "8",
                    "name": "Austria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "385",
            "name": "Olympic Games Football Tournament",
            "countries": [

            ],
            "federations": [
                {
                    "id": "8",
                    "name": "IOC"
                }
            ]
        },
        {
            "id": "19",
            "name": "OTP BANK Liga",
            "countries": [
                {
                    "id": "35",
                    "name": "Hungary"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "255",
            "name": "Paulista A1",
            "countries": [
                {
                    "id": "16",
                    "name": "Brazil"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "302",
            "name": "PFF Premiere League",
            "countries": [
                {
                    "id": "140",
                    "name": "Pakistan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "16",
            "name": "Premier",
            "countries": [
                {
                    "id": "15",
                    "name": "Belarus"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "11",
            "name": "Premier Division",
            "countries": [
                {
                    "id": "23",
                    "name": "Ireland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "2",
            "name": "Premier League",
            "countries": [
                {
                    "id": "19",
                    "name": "England"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "258",
            "name": "Premier League",
            "countries": [
                {
                    "id": "119",
                    "name": "Canada"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "7",
            "name": "Premier League",
            "countries": [
                {
                    "id": "12",
                    "name": "Russia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "273",
            "name": "Premier League",
            "countries": [
                {
                    "id": "142",
                    "name": "Ethiopia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "278",
            "name": "Premier League",
            "countries": [
                {
                    "id": "124",
                    "name": "Hong Kong"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "281",
            "name": "Premier League",
            "countries": [
                {
                    "id": "128",
                    "name": "Iraq"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "282",
            "name": "Premier League",
            "countries": [
                {
                    "id": "136",
                    "name": "Jamaica"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "30",
            "name": "Premier League",
            "countries": [
                {
                    "id": "27",
                    "name": "Thailand"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "31",
            "name": "Premier League",
            "countries": [
                {
                    "id": "28",
                    "name": "Azerbaijan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "287",
            "name": "Premier League",
            "countries": [
                {
                    "id": "89",
                    "name": "Jordan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "33",
            "name": "Premier League",
            "countries": [
                {
                    "id": "100",
                    "name": "Kuwait"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "36",
            "name": "Premier League",
            "countries": [
                {
                    "id": "74",
                    "name": "Egypt"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "292",
            "name": "Premier League",
            "countries": [
                {
                    "id": "135",
                    "name": "Lebanon"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "37",
            "name": "Premier League",
            "countries": [
                {
                    "id": "29",
                    "name": "Kenya"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "294",
            "name": "Premier League",
            "countries": [
                {
                    "id": "96",
                    "name": "Malaysia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "41",
            "name": "Premier League",
            "countries": [
                {
                    "id": "69",
                    "name": "South Africa"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "313",
            "name": "Premier League",
            "countries": [
                {
                    "id": "75",
                    "name": "Saudi Arabia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "64",
            "name": "Premier League",
            "countries": [
                {
                    "id": "13",
                    "name": "Ukraine"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "74",
            "name": "Premier League",
            "countries": [
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "80",
            "name": "Premier League",
            "countries": [
                {
                    "id": "91",
                    "name": "Tanzania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "341",
            "name": "Premier League",
            "countries": [
                {
                    "id": "134",
                    "name": "Syria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "86",
            "name": "Premier League",
            "countries": [
                {
                    "id": "99",
                    "name": "Ghana"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "88",
            "name": "Premier League",
            "countries": [
                {
                    "id": "147",
                    "name": "Sudan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "90",
            "name": "Premier League",
            "countries": [
                {
                    "id": "148",
                    "name": "Namibia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "91",
            "name": "Premier League",
            "countries": [
                {
                    "id": "180",
                    "name": "Zambia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "183",
            "name": "Premier League",
            "countries": [
                {
                    "id": "77",
                    "name": "Kazakhstan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "189",
            "name": "Premier League",
            "countries": [
                {
                    "id": "120",
                    "name": "Malta"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "238",
            "name": "Premier League",
            "countries": [
                {
                    "id": "190",
                    "name": "Armenia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "375",
            "name": "Premier League Asia Trophy",
            "countries": [

            ],
            "federations": [
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "85",
            "name": "Premier Soccer League",
            "countries": [
                {
                    "id": "113",
                    "name": "Zimbabwe"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "252",
            "name": "Premiere League",
            "countries": [
                {
                    "id": "127",
                    "name": "Bahrain"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "69",
            "name": "Premiership",
            "countries": [
                {
                    "id": "79",
                    "name": "Northern Ireland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "75",
            "name": "Premiership",
            "countries": [
                {
                    "id": "3",
                    "name": "Scotland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "70",
            "name": "Premijer Liga",
            "countries": [
                {
                    "id": "39",
                    "name": "Bosnia and Herzegovina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "18",
            "name": "Premium liiga",
            "countries": [
                {
                    "id": "25",
                    "name": "Estonia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "8",
            "name": "Primeira Liga",
            "countries": [
                {
                    "id": "32",
                    "name": "Portugal"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "51",
            "name": "Primera A",
            "countries": [
                {
                    "id": "61",
                    "name": "Colombia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "259",
            "name": "Primera B",
            "countries": [
                {
                    "id": "41",
                    "name": "Chile"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "265",
            "name": "Primera B",
            "countries": [
                {
                    "id": "61",
                    "name": "Colombia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "233",
            "name": "Primera B Metropolitana",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "96",
            "name": "Primera B Nacional",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "234",
            "name": "Primera C",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "235",
            "name": "Primera D",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "277",
            "name": "Primera Division",
            "countries": [
                {
                    "id": "59",
                    "name": "Guatemala"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "23",
            "name": "Primera Division",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "25",
            "name": "Primera División",
            "countries": [
                {
                    "id": "41",
                    "name": "Chile"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "299",
            "name": "Primera Division",
            "countries": [
                {
                    "id": "207",
                    "name": "Nicaragua"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "46",
            "name": "Primera División",
            "countries": [
                {
                    "id": "45",
                    "name": "Venezuela"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "47",
            "name": "Primera División",
            "countries": [
                {
                    "id": "52",
                    "name": "Peru"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "48",
            "name": "Primera División",
            "countries": [
                {
                    "id": "66",
                    "name": "Uruguay"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "52",
            "name": "Primera Division",
            "countries": [
                {
                    "id": "60",
                    "name": "Bolivia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "53",
            "name": "Primera División",
            "countries": [
                {
                    "id": "58",
                    "name": "Costa Rica"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "54",
            "name": "Primera Division",
            "countries": [
                {
                    "id": "63",
                    "name": "El Salvador"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "34",
            "name": "Pro League",
            "countries": [
                {
                    "id": "18",
                    "name": "Iran"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "301",
            "name": "Professional League",
            "countries": [
                {
                    "id": "122",
                    "name": "Oman"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "22",
            "name": "Prva Liga",
            "countries": [
                {
                    "id": "34",
                    "name": "Slovenia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "321",
            "name": "Prva Liga",
            "countries": [
                {
                    "id": "50",
                    "name": "Serbia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "200",
            "name": "Prva Liga",
            "countries": [
                {
                    "id": "51",
                    "name": "North Macedonia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "305",
            "name": "Qatar Stars League",
            "countries": [
                {
                    "id": "112",
                    "name": "Qatar"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "331",
            "name": "Recopa Sudamericana",
            "countries": [

            ],
            "federations": [
                {
                    "id": "6",
                    "name": "CONMEBOL"
                }
            ]
        },
        {
            "id": "168",
            "name": "Regionalliga",
            "countries": [
                {
                    "id": "1",
                    "name": "Germany"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "247",
            "name": "SAFF Championship",
            "countries": [

            ],
            "federations": [
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "105",
            "name": "Scottish Cup",
            "countries": [
                {
                    "id": "3",
                    "name": "Scotland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "140",
            "name": "Second Professional League",
            "countries": [
                {
                    "id": "38",
                    "name": "Bulgaria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "332",
            "name": "Segunda B",
            "countries": [
                {
                    "id": "43",
                    "name": "Spain"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "79",
            "name": "Segunda Division",
            "countries": [
                {
                    "id": "43",
                    "name": "Spain"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "92",
            "name": "Segunda Liga",
            "countries": [
                {
                    "id": "32",
                    "name": "Portugal"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "4",
            "name": "Serie A",
            "countries": [
                {
                    "id": "47",
                    "name": "Italy"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "24",
            "name": "Serie A",
            "countries": [
                {
                    "id": "16",
                    "name": "Brazil"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "50",
            "name": "Serie A",
            "countries": [
                {
                    "id": "56",
                    "name": "Ecuador"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "87",
            "name": "Serie B",
            "countries": [
                {
                    "id": "47",
                    "name": "Italy"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "95",
            "name": "Serie B",
            "countries": [
                {
                    "id": "16",
                    "name": "Brazil"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "181",
            "name": "Serie C",
            "countries": [
                {
                    "id": "47",
                    "name": "Italy"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "253",
            "name": "Serie C",
            "countries": [
                {
                    "id": "16",
                    "name": "Brazil"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "182",
            "name": "Serie C Super Cup",
            "countries": [
                {
                    "id": "47",
                    "name": "Italy"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "254",
            "name": "Serie D",
            "countries": [
                {
                    "id": "16",
                    "name": "Brazil"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "29",
            "name": "Sg. Premier League",
            "countries": [
                {
                    "id": "73",
                    "name": "Singapore"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "123",
            "name": "South African Cup",
            "countries": [
                {
                    "id": "69",
                    "name": "South Africa"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "248",
            "name": "Southeast Asian Games",
            "countries": [

            ],
            "federations": [
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "307",
            "name": "Stars League Cup",
            "countries": [
                {
                    "id": "112",
                    "name": "Qatar"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "261",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "41",
                    "name": "Chile"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "264",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "26",
                    "name": "China"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "286",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "72",
                    "name": "Japan"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "289",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "29",
                    "name": "Kenya"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "312",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "12",
                    "name": "Russia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "333",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "43",
                    "name": "Spain"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "348",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "48",
                    "name": "Turkey"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "349",
            "name": "Super Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "2",
                    "name": "UEFA"
                }
            ]
        },
        {
            "id": "352",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "13",
                    "name": "Ukraine"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "127",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "33",
                    "name": "Iceland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "129",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "15",
                    "name": "Belarus"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "137",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "2",
                    "name": "Belgium"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "139",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "38",
                    "name": "Bulgaria"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "143",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "49",
                    "name": "Cyprus"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "156",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "25",
                    "name": "Estonia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "160",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "21",
                    "name": "France"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "169",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "1",
                    "name": "Germany"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "176",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "35",
                    "name": "Hungary"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "178",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "47",
                    "name": "Italy"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "186",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "68",
                    "name": "Lithuania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "192",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "37",
                    "name": "Moldova"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "197",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "42",
                    "name": "Netherlands"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "205",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "6",
                    "name": "Norway"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "208",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "14",
                    "name": "Poland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "211",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "32",
                    "name": "Portugal"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "215",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "23",
                    "name": "Ireland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "219",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "36",
                    "name": "Romania"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "236",
            "name": "Super Cup",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "328",
            "name": "Super Cup MTN8",
            "countries": [
                {
                    "id": "69",
                    "name": "South Africa"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "9",
            "name": "Super League",
            "countries": [
                {
                    "id": "78",
                    "name": "Greece"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "15",
            "name": "Super League",
            "countries": [
                {
                    "id": "9",
                    "name": "Switzerland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "26",
            "name": "Super League",
            "countries": [
                {
                    "id": "26",
                    "name": "China"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "290",
            "name": "Super League",
            "countries": [
                {
                    "id": "29",
                    "name": "Kenya"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "293",
            "name": "Super League",
            "countries": [
                {
                    "id": "96",
                    "name": "Malaysia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "81",
            "name": "Super League",
            "countries": [
                {
                    "id": "98",
                    "name": "Uganda"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "94",
            "name": "Super League",
            "countries": [
                {
                    "id": "183",
                    "name": "Malawi"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "6",
            "name": "Super Lig",
            "countries": [
                {
                    "id": "48",
                    "name": "Turkey"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "62",
            "name": "Super Liga",
            "countries": [
                {
                    "id": "50",
                    "name": "Serbia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "296",
            "name": "Supercopa MX",
            "countries": [
                {
                    "id": "57",
                    "name": "Mexico"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "336",
            "name": "Superettan",
            "countries": [
                {
                    "id": "7",
                    "name": "Sweden"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "40",
            "name": "Superliga",
            "countries": [
                {
                    "id": "5",
                    "name": "Denmark"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "184",
            "name": "Superliga",
            "countries": [
                {
                    "id": "121",
                    "name": "Kosovo"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "376",
            "name": "Suruga Bank Championship",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "21",
            "name": "Telekom 1st CFL",
            "countries": [
                {
                    "id": "40",
                    "name": "Montenegro"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "170",
            "name": "Telekom Cup",
            "countries": [
                {
                    "id": "1",
                    "name": "Germany"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "232",
            "name": "Torneo Federal A",
            "countries": [
                {
                    "id": "65",
                    "name": "Argentina"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "237",
            "name": "Torneos de Verano",
            "countries": [

            ],
            "federations": [
                {
                    "id": "6",
                    "name": "CONMEBOL"
                }
            ]
        },
        {
            "id": "377",
            "name": "Toulon",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "354",
            "name": "UAE League",
            "countries": [
                {
                    "id": "76",
                    "name": "United Arab Emirates"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "387",
            "name": "UEFA EURO",
            "countries": [

            ],
            "federations": [
                {
                    "id": "2",
                    "name": "UEFA"
                }
            ]
        },
        {
            "id": "274",
            "name": "UEFA EURO Qualification",
            "countries": [

            ],
            "federations": [
                {
                    "id": "2",
                    "name": "UEFA"
                }
            ]
        },
        {
            "id": "350",
            "name": "UEFA Nations League",
            "countries": [

            ],
            "federations": [
                {
                    "id": "2",
                    "name": "UEFA"
                }
            ]
        },
        {
            "id": "378",
            "name": "Uhren Cup",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                }
            ]
        },
        {
            "id": "383",
            "name": "United Soccer League",
            "countries": [
                {
                    "id": "46",
                    "name": "USA"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "10",
            "name": "Urvalsdeild",
            "countries": [
                {
                    "id": "33",
                    "name": "Iceland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "118",
            "name": "US Open Cup",
            "countries": [
                {
                    "id": "46",
                    "name": "USA"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "384",
            "name": "USL League One",
            "countries": [
                {
                    "id": "46",
                    "name": "USA"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "39",
            "name": "V-League",
            "countries": [
                {
                    "id": "92",
                    "name": "Vietnam"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "380",
            "name": "V-League 2",
            "countries": [
                {
                    "id": "92",
                    "name": "Vietnam"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "57",
            "name": "Veikkausliiga",
            "countries": [
                {
                    "id": "22",
                    "name": "Finland"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "251",
            "name": "Victorian",
            "countries": [
                {
                    "id": "71",
                    "name": "Australia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "20",
            "name": "Virsliga",
            "countries": [
                {
                    "id": "24",
                    "name": "Latvia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "201",
            "name": "Vtora Liga",
            "countries": [
                {
                    "id": "51",
                    "name": "North Macedonia"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "104",
            "name": "Welsh Cup",
            "countries": [
                {
                    "id": "30",
                    "name": "Wales"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "303",
            "name": "West Bank Premier League",
            "countries": [
                {
                    "id": "179",
                    "name": "Palestine"
                }
            ],
            "federations": [

            ]
        },
        {
            "id": "358",
            "name": "World Cup AFC Qualifiers",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                },
                {
                    "id": "4",
                    "name": "AFC"
                }
            ]
        },
        {
            "id": "359",
            "name": "World Cup CAF Qualifiers",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                },
                {
                    "id": "3",
                    "name": "CAF"
                }
            ]
        },
        {
            "id": "360",
            "name": "World Cup CONCACAF Qualifiers",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                },
                {
                    "id": "5",
                    "name": "CONCACAF"
                }
            ]
        },
        {
            "id": "361",
            "name": "World Cup CONMEBOL Qualifiers",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                },
                {
                    "id": "6",
                    "name": "CONMEBOL"
                }
            ]
        },
        {
            "id": "365",
            "name": "World Cup Inter-Confederation Play-Off",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                },
                {
                    "id": "4",
                    "name": "AFC"
                },
                {
                    "id": "5",
                    "name": "CONCACAF"
                },
                {
                    "id": "6",
                    "name": "CONMEBOL"
                },
                {
                    "id": "7",
                    "name": "OFC"
                }
            ]
        },
        {
            "id": "364",
            "name": "World Cup OFC Qualifiers",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                },
                {
                    "id": "7",
                    "name": "OFC"
                }
            ]
        },
        {
            "id": "363",
            "name": "World Cup UEFA Qualifiers",
            "countries": [

            ],
            "federations": [
                {
                    "id": "1",
                    "name": "FIFA"
                },
                {
                    "id": "2",
                    "name": "UEFA"
                }
            ]
        },
        {
            "id": "158",
            "name": "Ykkonen",
            "countries": [
                {
                    "id": "22",
                    "name": "Finland"
                }
            ],
            "federations": [

            ]
        }
    ]
}
}
export default competitionJson
