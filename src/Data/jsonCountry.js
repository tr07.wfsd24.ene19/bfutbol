const countryJson = {
    "data": {
    "country": [
        {
            "id": "116",
            "name": "AFC",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=116",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=116"
        },
        {
            "id": "82",
            "name": "Africa",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=82",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=82"
        },
        {
            "id": "94",
            "name": "Africa Cup of Nations",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=94",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=94"
        },
        {
            "id": "102",
            "name": "Albania",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=102",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=102"
        },
        {
            "id": "67",
            "name": "Algeria",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=67",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=67"
        },
        {
            "id": "212",
            "name": "Andorra",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=212",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=212"
        },
        {
            "id": "132",
            "name": "Angola",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=132",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=132"
        },
        {
            "id": "65",
            "name": "Argentina",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=65",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=65"
        },
        {
            "id": "190",
            "name": "Armenia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=190",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=190"
        },
        {
            "id": "170",
            "name": "Aruba",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=170",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=170"
        },
        {
            "id": "83",
            "name": "Asia",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=83",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=83"
        },
        {
            "id": "101",
            "name": "Asian Cup",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=101",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=101"
        },
        {
            "id": "71",
            "name": "Australia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=71",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=71"
        },
        {
            "id": "8",
            "name": "Austria",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=8",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=8"
        },
        {
            "id": "28",
            "name": "Azerbaijan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=28",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=28"
        },
        {
            "id": "127",
            "name": "Bahrain",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=127",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=127"
        },
        {
            "id": "209",
            "name": "Bangladesh",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=209",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=209"
        },
        {
            "id": "15",
            "name": "Belarus",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=15",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=15"
        },
        {
            "id": "2",
            "name": "Belgium",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=2",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=2"
        },
        {
            "id": "151",
            "name": "Benin",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=151",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=151"
        },
        {
            "id": "199",
            "name": "Bhutan",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=199",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=199"
        },
        {
            "id": "60",
            "name": "Bolivia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=60",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=60"
        },
        {
            "id": "39",
            "name": "Bosnia and Herzegovina",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=39",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=39"
        },
        {
            "id": "150",
            "name": "Botswana",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=150",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=150"
        },
        {
            "id": "16",
            "name": "Brazil",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=16",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=16"
        },
        {
            "id": "38",
            "name": "Bulgaria",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=38",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=38"
        },
        {
            "id": "156",
            "name": "Burkina Faso",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=156",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=156"
        },
        {
            "id": "165",
            "name": "Burundi",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=165",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=165"
        },
        {
            "id": "118",
            "name": "CAF",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=118",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=118"
        },
        {
            "id": "203",
            "name": "Cambodia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=203",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=203"
        },
        {
            "id": "144",
            "name": "Cameroon",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=144",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=144"
        },
        {
            "id": "119",
            "name": "Canada",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=119",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=119"
        },
        {
            "id": "171",
            "name": "Carribean Netherlands",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=171",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=171"
        },
        {
            "id": "159",
            "name": "Central African Republic",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=159",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=159"
        },
        {
            "id": "158",
            "name": "Chad",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=158",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=158"
        },
        {
            "id": "84",
            "name": "Champions League",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=84",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=84"
        },
        {
            "id": "41",
            "name": "Chile",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=41",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=41"
        },
        {
            "id": "26",
            "name": "China",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=26",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=26"
        },
        {
            "id": "61",
            "name": "Colombia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=61",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=61"
        },
        {
            "id": "168",
            "name": "Comoros",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=168",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=168"
        },
        {
            "id": "93",
            "name": "CONCACAF",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=93",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=93"
        },
        {
            "id": "138",
            "name": "CONCACAF Nations League",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=138",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=138"
        },
        {
            "id": "107",
            "name": "Confederations Cup",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=107",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=107"
        },
        {
            "id": "177",
            "name": "Congo",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=177",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=177"
        },
        {
            "id": "117",
            "name": "CONMEBOL",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=117",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=117"
        },
        {
            "id": "187",
            "name": "Copa America",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=187",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=187"
        },
        {
            "id": "58",
            "name": "Costa Rica",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=58",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=58"
        },
        {
            "id": "54",
            "name": "Croatia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=54",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=54"
        },
        {
            "id": "172",
            "name": "Curacao",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=172",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=172"
        },
        {
            "id": "49",
            "name": "Cyprus",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=49",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=49"
        },
        {
            "id": "11",
            "name": "Czech Republic",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=11",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=11"
        },
        {
            "id": "5",
            "name": "Denmark",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=5",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=5"
        },
        {
            "id": "163",
            "name": "Djibouti",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=163",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=163"
        },
        {
            "id": "200",
            "name": "Dominican Republic",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=200",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=200"
        },
        {
            "id": "131",
            "name": "DR Congo",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=131",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=131"
        },
        {
            "id": "56",
            "name": "Ecuador",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=56",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=56"
        },
        {
            "id": "74",
            "name": "Egypt",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=74",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=74"
        },
        {
            "id": "63",
            "name": "El Salvador",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=63",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=63"
        },
        {
            "id": "19",
            "name": "England",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=19",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=19"
        },
        {
            "id": "185",
            "name": "Equatorial Guinea",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=185",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=185"
        },
        {
            "id": "157",
            "name": "Eritrea",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=157",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=157"
        },
        {
            "id": "25",
            "name": "Estonia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=25",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=25"
        },
        {
            "id": "178",
            "name": "Estwani",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=178",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=178"
        },
        {
            "id": "142",
            "name": "Ethiopia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=142",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=142"
        },
        {
            "id": "206",
            "name": "EURO",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=206",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=206"
        },
        {
            "id": "88",
            "name": "Europa League",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=88",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=88"
        },
        {
            "id": "214",
            "name": "Faroe Islands",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=214",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=214"
        },
        {
            "id": "105",
            "name": "Fiji",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=105",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=105"
        },
        {
            "id": "22",
            "name": "Finland",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=22",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=22"
        },
        {
            "id": "21",
            "name": "France",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=21",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=21"
        },
        {
            "id": "173",
            "name": "French Guiana",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=173",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=173"
        },
        {
            "id": "154",
            "name": "Gabon",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=154",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=154"
        },
        {
            "id": "161",
            "name": "Gambia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=161",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=161"
        },
        {
            "id": "17",
            "name": "Georgia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=17",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=17"
        },
        {
            "id": "1",
            "name": "Germany",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=1",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=1"
        },
        {
            "id": "99",
            "name": "Ghana",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=99",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=99"
        },
        {
            "id": "213",
            "name": "Gibraltar",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=213",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=213"
        },
        {
            "id": "78",
            "name": "Greece",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=78",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=78"
        },
        {
            "id": "59",
            "name": "Guatemala",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=59",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=59"
        },
        {
            "id": "174",
            "name": "Guiana",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=174",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=174"
        },
        {
            "id": "133",
            "name": "Guinea",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=133",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=133"
        },
        {
            "id": "64",
            "name": "Honduras",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=64",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=64"
        },
        {
            "id": "124",
            "name": "Hong Kong",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=124",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=124"
        },
        {
            "id": "35",
            "name": "Hungary",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=35",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=35"
        },
        {
            "id": "33",
            "name": "Iceland",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=33",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=33"
        },
        {
            "id": "87",
            "name": "India",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=87",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=87"
        },
        {
            "id": "90",
            "name": "Indonesia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=90",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=90"
        },
        {
            "id": "4",
            "name": "International",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=4",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=4"
        },
        {
            "id": "18",
            "name": "Iran",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=18",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=18"
        },
        {
            "id": "128",
            "name": "Iraq",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=128",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=128"
        },
        {
            "id": "23",
            "name": "Ireland",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=23",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=23"
        },
        {
            "id": "55",
            "name": "Israel",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=55",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=55"
        },
        {
            "id": "47",
            "name": "Italy",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=47",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=47"
        },
        {
            "id": "146",
            "name": "Ivory Coast",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=146",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=146"
        },
        {
            "id": "136",
            "name": "Jamaica",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=136",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=136"
        },
        {
            "id": "72",
            "name": "Japan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=72",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=72"
        },
        {
            "id": "89",
            "name": "Jordan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=89",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=89"
        },
        {
            "id": "77",
            "name": "Kazakhstan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=77",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=77"
        },
        {
            "id": "29",
            "name": "Kenya",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=29",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=29"
        },
        {
            "id": "121",
            "name": "Kosovo",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=121",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=121"
        },
        {
            "id": "100",
            "name": "Kuwait",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=100",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=100"
        },
        {
            "id": "205",
            "name": "Kyrgyzstan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=205",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=205"
        },
        {
            "id": "204",
            "name": "Laos",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=204",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=204"
        },
        {
            "id": "24",
            "name": "Latvia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=24",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=24"
        },
        {
            "id": "135",
            "name": "Lebanon",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=135",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=135"
        },
        {
            "id": "166",
            "name": "Lesotho",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=166",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=166"
        },
        {
            "id": "155",
            "name": "Liberia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=155",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=155"
        },
        {
            "id": "181",
            "name": "Libya",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=181",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=181"
        },
        {
            "id": "68",
            "name": "Lithuania",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=68",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=68"
        },
        {
            "id": "10",
            "name": "Luxembourg",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=10",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=10"
        },
        {
            "id": "145",
            "name": "Madagascar",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=145",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=145"
        },
        {
            "id": "183",
            "name": "Malawi",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=183",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=183"
        },
        {
            "id": "96",
            "name": "Malaysia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=96",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=96"
        },
        {
            "id": "143",
            "name": "Mali",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=143",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=143"
        },
        {
            "id": "120",
            "name": "Malta",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=120",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=120"
        },
        {
            "id": "162",
            "name": "Mauritania",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=162",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=162"
        },
        {
            "id": "149",
            "name": "Mautitius",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=149",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=149"
        },
        {
            "id": "57",
            "name": "Mexico",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=57",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=57"
        },
        {
            "id": "37",
            "name": "Moldova",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=37",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=37"
        },
        {
            "id": "197",
            "name": "Mongolia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=197",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=197"
        },
        {
            "id": "40",
            "name": "Montenegro",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=40",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=40"
        },
        {
            "id": "81",
            "name": "Morocco",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=81",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=81"
        },
        {
            "id": "184",
            "name": "Mozambique",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=184",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=184"
        },
        {
            "id": "189",
            "name": "Myanmar",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=189",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=189"
        },
        {
            "id": "148",
            "name": "Namibia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=148",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=148"
        },
        {
            "id": "208",
            "name": "Nepal",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=208",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=208"
        },
        {
            "id": "42",
            "name": "Netherlands",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=42",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=42"
        },
        {
            "id": "194",
            "name": "New Caledonia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=194",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=194"
        },
        {
            "id": "114",
            "name": "New Zealand",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=114",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=114"
        },
        {
            "id": "207",
            "name": "Nicaragua",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=207",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=207"
        },
        {
            "id": "160",
            "name": "Niger",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=160",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=160"
        },
        {
            "id": "108",
            "name": "Nigeria",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=108",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=108"
        },
        {
            "id": "129",
            "name": "North Korea",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=129",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=129"
        },
        {
            "id": "51",
            "name": "North Macedonia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=51",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=51"
        },
        {
            "id": "79",
            "name": "Northern Ireland",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=79",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=79"
        },
        {
            "id": "6",
            "name": "Norway",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=6",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=6"
        },
        {
            "id": "137",
            "name": "Oceania",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=137",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=137"
        },
        {
            "id": "31",
            "name": "Olympics Men",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=31",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=31"
        },
        {
            "id": "122",
            "name": "Oman",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=122",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=122"
        },
        {
            "id": "140",
            "name": "Pakistan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=140",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=140"
        },
        {
            "id": "179",
            "name": "Palestine",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=179",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=179"
        },
        {
            "id": "95",
            "name": "Panama",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=95",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=95"
        },
        {
            "id": "196",
            "name": "Papua New Guinea",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=196",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=196"
        },
        {
            "id": "62",
            "name": "Paraguay",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=62",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=62"
        },
        {
            "id": "52",
            "name": "Peru",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=52",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=52"
        },
        {
            "id": "202",
            "name": "Philippines",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=202",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=202"
        },
        {
            "id": "14",
            "name": "Poland",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=14",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=14"
        },
        {
            "id": "32",
            "name": "Portugal",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=32",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=32"
        },
        {
            "id": "112",
            "name": "Qatar",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=112",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=112"
        },
        {
            "id": "70",
            "name": "Republic of Korea",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=70",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=70"
        },
        {
            "id": "169",
            "name": "Reunion",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=169",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=169"
        },
        {
            "id": "36",
            "name": "Romania",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=36",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=36"
        },
        {
            "id": "12",
            "name": "Russia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=12",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=12"
        },
        {
            "id": "97",
            "name": "Rwanda",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=97",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=97"
        },
        {
            "id": "201",
            "name": "Samoa",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=201",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=201"
        },
        {
            "id": "139",
            "name": "San Marino",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=139",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=139"
        },
        {
            "id": "75",
            "name": "Saudi Arabia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=75",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=75"
        },
        {
            "id": "3",
            "name": "Scotland",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=3",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=3"
        },
        {
            "id": "103",
            "name": "Senegal",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=103",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=103"
        },
        {
            "id": "50",
            "name": "Serbia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=50",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=50"
        },
        {
            "id": "152",
            "name": "Seychelles",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=152",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=152"
        },
        {
            "id": "164",
            "name": "Sierra Leone",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=164",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=164"
        },
        {
            "id": "73",
            "name": "Singapore",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=73",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=73"
        },
        {
            "id": "53",
            "name": "Slovakia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=53",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=53"
        },
        {
            "id": "34",
            "name": "Slovenia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=34",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=34"
        },
        {
            "id": "211",
            "name": "Solomon Islands",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=211",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=211"
        },
        {
            "id": "69",
            "name": "South Africa",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=69",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=69"
        },
        {
            "id": "44",
            "name": "South America",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=44",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=44"
        },
        {
            "id": "167",
            "name": "South Sudan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=167",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=167"
        },
        {
            "id": "43",
            "name": "Spain",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=43",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=43"
        },
        {
            "id": "198",
            "name": "Sri Lanka",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=198",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=198"
        },
        {
            "id": "147",
            "name": "Sudan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=147",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=147"
        },
        {
            "id": "175",
            "name": "Suriname",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=175",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=175"
        },
        {
            "id": "186",
            "name": "Swaziland",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=186",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=186"
        },
        {
            "id": "7",
            "name": "Sweden",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=7",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=7"
        },
        {
            "id": "9",
            "name": "Switzerland",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=9",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=9"
        },
        {
            "id": "134",
            "name": "Syria",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=134",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=134"
        },
        {
            "id": "193",
            "name": "Tahiti",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=193",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=193"
        },
        {
            "id": "210",
            "name": "Taiwan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=210",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=210"
        },
        {
            "id": "191",
            "name": "Tajikistan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=191",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=191"
        },
        {
            "id": "91",
            "name": "Tanzania",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=91",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=91"
        },
        {
            "id": "27",
            "name": "Thailand",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=27",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=27"
        },
        {
            "id": "153",
            "name": "Togo",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=153",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=153"
        },
        {
            "id": "176",
            "name": "Trinidad and Tobago",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=176",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=176"
        },
        {
            "id": "80",
            "name": "Tunisia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=80",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=80"
        },
        {
            "id": "48",
            "name": "Turkey",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=48",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=48"
        },
        {
            "id": "130",
            "name": "Turkmenistan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=130",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=130"
        },
        {
            "id": "115",
            "name": "UEFA",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=115",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=115"
        },
        {
            "id": "125",
            "name": "UEFA Nations League",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=125",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=125"
        },
        {
            "id": "98",
            "name": "Uganda",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=98",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=98"
        },
        {
            "id": "13",
            "name": "Ukraine",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=13",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=13"
        },
        {
            "id": "76",
            "name": "United Arab Emirates",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=76",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=76"
        },
        {
            "id": "66",
            "name": "Uruguay",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=66",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=66"
        },
        {
            "id": "46",
            "name": "USA",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=46",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=46"
        },
        {
            "id": "188",
            "name": "Uzbekistan",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=188",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=188"
        },
        {
            "id": "195",
            "name": "Vanuatu",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=195",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=195"
        },
        {
            "id": "45",
            "name": "Venezuela",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=45",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=45"
        },
        {
            "id": "92",
            "name": "Vietnam",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=92",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=92"
        },
        {
            "id": "30",
            "name": "Wales",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=30",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=30"
        },
        {
            "id": "85",
            "name": "World Cup",
            "is_real": "0",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=85",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=85"
        },
        {
            "id": "180",
            "name": "Zambia",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=180",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=180"
        },
        {
            "id": "182",
            "name": "Zanzibar",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=182",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=182"
        },
        {
            "id": "113",
            "name": "Zimbabwe",
            "is_real": "1",
            "leagues": "https://livescore-api.com/api-client/leagues/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=113",
            "scores": "https://livescore-api.com/api-client/scores/live.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&country=113"
        }
    ]
}
}
export default countryJson
