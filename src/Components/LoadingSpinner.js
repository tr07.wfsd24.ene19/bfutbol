import React from 'react';

const LoadingSpinner = () => (
    <div className="mt-4">
        <i className="fa fa-spinner fa-spin" /> Cargando datos...
    </div>
);

export default LoadingSpinner;
