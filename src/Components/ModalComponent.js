import React from 'react';
import {translate} from "../mixed/functTranslate";

function ModalComponent({datos,equipos}) {

    const {player,time,event,home_away} = datos
    const {h,a} = equipos

    let eqEvento;
    if (home_away === 'h') {
        eqEvento = h
    } else if (home_away === 'a') {
        eqEvento = a
    }

    return (
            <tr>
                <th>Min. {time}</th>
                <th>{player}</th>
                <th>{eqEvento}</th>
                <th>{translate(event)}</th>
            </tr>
    );
}

export default ModalComponent;
