import React, {useState, useEffect, useContext} from 'react';
import axios from 'axios'
import LoadingSpinner from "./LoadingSpinner";
import Livescoresult from "./Livescoreresult";
import {LeagueIdContext} from '../Contextos'
import {liveScoresURI} from '../Data/urls'

function Livescores({loadLiveScore, loadAll, checkMode}) {

    const [data, setData] = useState([]);
    const [texto, setTexto] = useState("");
    const [loading, setLoad] = useState(false);

    const {leagueData, countryData} = useContext(LeagueIdContext)


    useEffect(() => {

            if (loadLiveScore && leagueData.name && !loadAll) {
                getData(`&competition_id=${leagueData.id}`)
            } else if (loadLiveScore && (!leagueData.name || loadAll)) {
                getData(`&country=${countryData.id}`)
            }

        },
        [loadLiveScore, countryData.name, countryData.id, loadAll],
    );

    let changeModeAll = () => {
        checkMode(true)
        getData(`&country=${countryData.id}`)
    }

    let changeModeSingleLeague = () => {
        checkMode(false)
        getData(`&competition_id=${leagueData.id}`)
    }

    let getData = (add) => {

        let url = liveScoresURI + add
        setLoad(true);

        axios.get(url)
            .then(function (response) {
                let {data} = response.data;
                setData(data.match);
                setLoad(false);
                console.log(data)

                if (!data.length > 0) {

                    setTexto('No hay partidos en directo');
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    let checkDataResult = () => {

        if (data.length > 0) {
            return (
                <table className="uk-table uk-table-striped uk-table-responsive">
                    <thead>
                    <tr>
                        <th className="text-center uk-text-secondary">Competición</th>
                        <th className="text-center uk-text-secondary">Inicio</th>
                        <th className="text-center uk-text-secondary">Local</th>
                        <th className="text-center uk-text-secondary">Marcador</th>
                        <th className="text-center uk-text-secondary">Visitante</th>
                        <th className="text-center uk-text-secondary">Tiempo</th>
                        <th className="text-center uk-text-secondary">Estado</th>
                        <th className="text-center uk-text-secondary">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.keys(data).map(key => (

                        <Livescoresult datos={data[key]} key={key}/>

                    ))}
                    </tbody>
                </table>
            )
        } else {
            return <p className="text-danger pt-4">{texto}</p>
        }
    }

    return (
        <React.Fragment>
            {countryData.id && countryData.name !== '' && (<div>
                <h2 className="mb-3 pt-2 pb-2">Resultados en directo</h2>
                <button disabled={!leagueData.id || leagueData.id === '0' ? true : null} type="button"
                        onClick={() => changeModeSingleLeague()}
                        className={(leagueData.name && !loadAll) ? "btn btn-success" : 'btn btn-outline-secondary'}>Por
                    competición
                </button>
                <button type="button" onClick={() => changeModeAll()}
                        className={(!leagueData.name || loadAll) ? "ml-2 btn btn-success" : 'ml-2 btn btn-outline-secondary'}>Todas
                    las competiciones
                </button>
                {loading ? <LoadingSpinner/> : checkDataResult()}
            </div>)}
        </React.Fragment>
    );
}

export default Livescores;
