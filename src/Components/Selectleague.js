import axios from 'axios';
import React, {useState, Fragment,useContext,useEffect} from 'react';
import {LeagueIdContext} from '../Contextos'
import countryJson from "../Data/jsonCountry";

function Selectleague(props) {

    const [leagueDatos, setLeague] = useState([{name:'',id:''}]);
    const [countryDatos, setCountry] = useState([{name:'',id:'',scores:""}]);
    const [dataCompe, setDataCompe] = useState([]);
    const {checkData} = useContext(LeagueIdContext)

    const urlDefault = `https://livescore-api.com/api-client/competitions/list.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5`

    useEffect( () => {

        // pasar contexto
            checkData(leagueDatos,countryDatos)
        },
        [leagueDatos,countryDatos]
    );

    let getDataCompe = async (uri,id) => {

        await axios.get(uri+`&country_id=${id}`)
            .then(function (response) {
                let {data} = response.data;
                setDataCompe(data.competition);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    let changeLeague = (e) => {

        //leer del option el data id y el value
        let el = e.target.childNodes[e.target.selectedIndex]
        let idLeague =  el.getAttribute('id');

        //setear estado
        setLeague({name:e.target.value,id:idLeague})
    }

    let changeCountry = (e) => {

        //leer del option el data id y el value
        let el = e.target.childNodes[e.target.selectedIndex]
        let idCountry = el.getAttribute('id');

        let el1 = e.target.childNodes[e.target.selectedIndex]
        let scores = el1.getAttribute('data-id');

        setCountry({name: e.target.value, id: idCountry,scores:scores})
        getDataCompe(urlDefault,idCountry)

    }

    let {data} = countryJson

    return (
        <Fragment>
            <div className="mt-3 mb-4">
                <form className="mb-2">
                    <div className="input-group mb-3">
                        <select onChange={e => changeCountry(e)} className="custom-select"
                                id="inputGroupSelect02" required>
                            <option id='0' value=''>Selecciona un pais</option>
                            {Object.keys(data.country).map((key,index) => (
                                <option key={index} id={data.country[key].id} data-id={data.country[key].scores}
                                        value={data.country[key].name}>{data.country[key].name}</option>
                            ))}
                        </select>
                        <select onChange={e => changeLeague(e)} className="custom-select ml-2"
                                id="inputGroupSelect01" required disabled={!countryDatos.id || countryDatos.id==='0' ? true:null}>
                            <option id='0' value=''>Selecciona una competeción</option>
                            {Object.keys(dataCompe).map((key,index) => (
                                <option key={index} id={dataCompe[key].id}
                                        value={dataCompe[key].name}>{dataCompe[key].name}</option>
                            ))}
                        </select>
                    </div>
                </form>
            </div>
        </Fragment>
    );
}

export default Selectleague;
