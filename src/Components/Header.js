import React,{useContext} from 'react';
import img from "../img/logo.png"
import {NavLink} from "react-router-dom";
import {LeagueIdContext} from '../Contextos'

function Header () {

    return (
        <nav className="navbar navbar-expand-lg navbar-light pl-0 pr-0">
            <div className="d-flex justify-content-start align-items-center">
                <img src={img} width="60" height="60" className="d-inline-block align-top mr-2" alt=""/>
                <a href="/">
                    <p className="mr-5 ">B<span className="text-success font-weight-bold">.</span>Futbol</p>
                </a>
            </div>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse nav-tabs" id="navbarSupportedContent">
                    <ul className="nav">
                        <li className="nav-item">
                            <NavLink to="/fixtures" className="nav-link text-secondary" activeClassName="text-dark active">Próximos
                                partidos</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/livescores" className="nav-link text-secondary" activeClassName="text-dark active">Resultados en directo</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/pastmatches" className="nav-link text-secondary" activeClassName="text-dark active">Partidos finalizados</NavLink>
                        </li>
                    </ul>
                </div>
        </nav>
    )
};

export default Header;
