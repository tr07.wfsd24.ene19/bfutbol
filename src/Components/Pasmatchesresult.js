import React from 'react';

function Pastmatchesresult({datos}) {

    const {score, home_name, away_name, date,scheduled,competition_name} = datos

    //format fecha
    let fecha = new Date(date);
    let options = {year: 'numeric', month: 'short', day: 'numeric'};

    //time, suma 2 horas, diferencia horaria con api time
    let res = scheduled.substr(0, 2);
    let res2 = scheduled.substr(2, 3);
    let newTime = +res + +'2' + res2

    return (
        <tr>
            <th className="text-center">{competition_name}</th>
            <th className="text-center">{fecha.toLocaleDateString("es-ES", options) + ' - ' + newTime}</th>
            <th className="text-center">{home_name}</th>
            <th className="text-center">{score}</th>
            <th className="text-center">{away_name}</th>
        </tr>
    );
}

export default Pastmatchesresult;
