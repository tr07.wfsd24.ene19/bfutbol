import React, {useContext, Fragment} from 'react';
import axios from 'axios';
import {LeagueIdContext} from '../Contextos'
import {translate2} from "../mixed/functTranslate";

const live = function Livescoresult({datos}) {

    const {dataModalLiveScore} = useContext(LeagueIdContext)

    const {score,home_name,away_name,time,status,scheduled,id,events,competition_name} = datos
    let urlDetail = `https://livescore-api.com/api-client/scores/events.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&id=${id}`

    //time, suma 2 horas, diferencia horaria con api time
    let res = scheduled.substr(0, 2);
    let res2 = scheduled.substr(2, 3);
    let newTime = +res+ +'2'+res2

    //coger eventos
    let getData = async (uri) => {

        await axios.get(uri)
            .then(function (response) {
                let {data} = response.data;

                //context component app para evento MODAL
                if(events !==false) {
                    dataModalLiveScore(data.event,home_name,away_name,score)
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    return (
        <Fragment>
            <tr>
                <th className="text-center">{competition_name}</th>
                <th className="text-center">{newTime}</th>
                <th className="text-center">{home_name}</th>
                <th className="text-center">{score}</th>
                <th className="text-center">{away_name}</th>
                <th className="text-center">Min. {time}</th>
                <th className={status === 'IN PLAY' ? 'text-center uk-text-success':'text-center'}>{translate2(status)}</th>
                <th className="text-center">{events !==false && (<button type="button" onClick={()=>getData(urlDetail)}
                                        className="btn btn-outline-secondary btnB">Detalles</button>)}</th>
            </tr>
        </Fragment>
    );
}

export default React.memo(live);
