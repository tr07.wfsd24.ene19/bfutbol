import React from 'react';

function Fixturesresult({datos}) {

    const {date,home_name,away_name,location,time,round} = datos

    //format fecha
    let fecha = new Date(date);
    let options = { year: 'numeric', month: 'short', day: 'numeric' };

    //time, suma 2 horas, diferencia horaria con api time
    let res = time.substr(0, 2);
    let res2 = time.substr(2, 3);
    let newTime = +res+ +'2'+res2

    return (
        <tr>
            <th className="text-center">{round}</th>
            <th className="text-center">{fecha.toLocaleDateString("es-ES", options)+' - '+newTime}</th>
            <th className="text-center">{home_name}</th>
            <th className="text-center">{away_name}</th>
            <th className="text-center">{location}</th>
        </tr>
    );
}

export default Fixturesresult;
