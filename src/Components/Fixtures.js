import React, {useState, useContext, Fragment} from 'react';
import axios from 'axios';
import Fixturesresult from "./Fixturesresult";
import LoadingSpinner from "./LoadingSpinner";
import {LeagueIdContext} from '../Contextos'
import {fixtureURI} from "../Data/urls";

function Fixtures() {

    const [data, setData] = useState([]);
    const [dataNext, setDataNext] = useState([]);
    const [dataPrev, setDataPrev] = useState([]);
    const [fecha, setFecha] = useState("");
    const [texto, setTexto] = useState("");
    const [loading, setLoad] = useState(false);
    const {leagueData} = useContext(LeagueIdContext)

    function handleOnSubmit(e) {
        e.preventDefault();

        //set data
        setData([]);
        setTexto('')

        let urlMod = fixtureURI+leagueData.id+`&date=${fecha}`;

        if (fecha) {
            getData(urlMod)
        }
    }

    let getData = async (uri) => {

        setLoad(true);

        await axios.get(uri)
            .then(function (response) {
                let {data} = response.data;
                setData(data.fixtures);
                setDataNext(data.next_page);
                setDataPrev(data.prev_page);
                setLoad(false);

                console.log(data.fixtures)

                if (!data.length > 0) {

                    setTexto('No se han encontrado datos');
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    let showAllresult = () => {
        setFecha('')
        getData(fixtureURI+leagueData.id)
    }

    let checkDataResult = () => {

        if (data.length > 0) {
            return (
                <Fragment>
                    <table className="uk-table uk-table-striped uk-table-responsive">
                        <thead>
                        <tr>
                            <th className="uk-text-secondary text-center">Jornada</th>
                            <th className="uk-text-secondary text-center">Fecha / Hora</th>
                            <th className="uk-text-secondary text-center">Local</th>
                            <th className="uk-text-secondary text-center">Visitante</th>
                            <th className="uk-text-secondary text-center">Estadio</th>
                        </tr>
                        </thead>
                        <tbody>
                        {Object.keys(data).map(key => (

                            <Fixturesresult datos={data[key]} key={key}/>

                        ))}
                        </tbody>
                    </table>
                    <nav className="nav mb-5">
                        {dataPrev !== false && (<button onClick={() => getData(dataPrev)} type="button"
                                                        className="btn btn-outline-secondary mr-4">&lt; Página
                            anterior</button>)}
                        {dataNext !== false && (<button onClick={() => getData(dataNext)} type="button"
                                                        className="btn btn-outline-secondary">Página
                            siguiente &gt;</button>)}
                    </nav>
                </Fragment>
            )
        } else {
            return <p className="pt-3 text-danger">{texto}</p>
        }
    }

    return (
        <Fragment>
            {leagueData.name && (<div>
                    <h2 className="pt-2">Próximos partidos</h2>
                    <form onSubmit={handleOnSubmit} className="form-inline">
                        <div className="form-group mt-3 mb-2">
                            <label className="pr-2">Por Fecha</label>
                            <input
                                type="date"
                                className="form-control mr-2"
                                name="fecha"
                                onChange={e => setFecha(e.target.value)}
                                value={fecha}
                                required
                            />
                            <button type="submit" className="btn btn-outline-secondary">Buscar</button>
                            <button onClick={() => showAllresult()} type="button"
                                    className="btn btn-outline-secondary ml-4">Listar todos
                            </button>
                        </div>
                    </form>
                    {loading ? <LoadingSpinner/> : checkDataResult()}
                </div>
            )}
        </Fragment>
    );
}

export default Fixtures;
