import React, {useState, useContext, Fragment} from 'react';
import axios from 'axios';
import LoadingSpinner from "./LoadingSpinner";
import {LeagueIdContext} from '../Contextos'
import Pastmatchesresult from "./Pasmatchesresult";

function Pastmatches() {

    const [data, setData] = useState([]);
    const [dataNext, setDataNext] = useState([]);
    const [dataPrev, setDataPrev] = useState([]);
    const [fecha, setFecha] = useState("");
    const [fecha2, setFecha2] = useState("");
    const [texto, setTexto] = useState("");
    const [loading, setLoad] = useState(false);

    const {leagueData} = useContext(LeagueIdContext)
    const urlDefault = `https://livescore-api.com/api-client/scores/history.json?key=4b7NKQ88N1wQ0zUx&secret=V8PDSWSQMO309Z1ea81VYa4McACcaxJ5&competition_id=${leagueData.id}`

    function handleOnSubmit(e) {
        e.preventDefault();

        setData([]);
        setTexto('')

        let urlMod = urlDefault + `&from=${fecha}&to=${fecha2}`

        if (Date.parse(fecha) > Date.parse(fecha2)) {
            setTexto('La fecha inicial es mayor');
        } else {

            getData(urlMod)
        }
    }

    let getData = async (uri) => {

        setLoad(true);

        await axios.get(uri)
            .then(function (response) {
                let {data} = response.data;
                setData(data.match);
                setDataNext(data.next_page);
                setDataPrev(data.prev_page);
                setLoad(false);

                console.log(data.match)

                if (!data.length > 0) {

                    setTexto('No se han encontrado datos');
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    let checkDataResult = () => {

        if (data.length > 0) {
            return (
                <Fragment>
                    <table className="uk-table uk-table-striped uk-table-responsive">
                        <thead>
                        <tr>
                            <th className="text-center uk-text-secondary">Competición</th>
                            <th className="text-center uk-text-secondary">Fecha / Hora</th>
                            <th className="text-center uk-text-secondary">Local</th>
                            <th className="text-center uk-text-secondary">Marcador</th>
                            <th className="text-center uk-text-secondary">Visitante</th>
                        </tr>
                        </thead>
                        <tbody>
                        {Object.keys(data).map(key => (

                            <Pastmatchesresult datos={data[key]} key={key}/>

                        ))}
                        </tbody>
                    </table>
                    <nav className="nav mb-5">
                        {dataPrev !== false && (
                            <button onClick={() => getData(dataPrev.replace(/&amp;/g, "&"))} type="button"
                                    className="btn btn-outline-secondary mr-4">&lt; Página
                                anterior</button>)}
                        {dataNext !== false && (
                            <button onClick={() => getData(dataNext.replace(/&amp;/g, "&"))} type="button"
                                    className="btn btn-outline-secondary">Página
                                siguiente &gt;</button>)}
                    </nav>
                </Fragment>
            )
        } else {
            return <p className="mt-3 text-danger">{texto}</p>
        }
    }

    return (
        <Fragment>
            {leagueData.name && (<div>
                    <h2 className="pt-2">Partidos Finalizados</h2>
                    <form onSubmit={handleOnSubmit} className="form-inline">
                        <div className="form-group mt-3 mb-2">
                            <label className="pr-2">Desde</label>
                            <input
                                type="date"
                                className="form-control mr-2"
                                name="fecha"
                                onChange={e => setFecha(e.target.value)}
                                value={fecha}
                                required
                            />
                            <label className="pr-2">Hasta</label>
                            <input
                                type="date"
                                className="form-control mr-2"
                                name="fecha"
                                onChange={e => setFecha2(e.target.value)}
                                value={fecha2}
                                required
                            />
                            <button type="submit" className="btn btn-outline-secondary">Buscar</button>
                        </div>
                    </form>
                    {loading ? <LoadingSpinner/> : checkDataResult()}
                </div>
            )}
        </Fragment>
    );
}

export default Pastmatches;
