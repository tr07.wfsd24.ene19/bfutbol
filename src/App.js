import React, {Fragment,useState} from 'react';
import './App.css';
import Fixtures from "./Components/Fixtures";
import Header from "./Components/Header";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Livescores from "./Components/Livescores";
import Selectleague from "./Components/Selectleague";
import {LeagueIdContext} from './Contextos'
import Modal from 'react-responsive-modal';
import ModalComponent from "./Components/ModalComponent";
import Pastmatches from "./Components/Pastmatches";

function App() {

    const [leagueData, setLeague] = useState([]);
    const [countryData, setCountry] = useState([]);
    const [dataEvent, setDataEvent] = useState({eventos:[]});
    const [equiposData, setDataequipos] = useState({h:"",a:"",score:""});
    const [openModal, setOpen] = useState(false);

    //estados de livescore
    const [loadAll, setLoadAll] = useState('');
    const [loadLiveScore, setLoadLiveScore] = useState(true);

    //funcion lanzada por context
    let checkData = (league,country) => {
        setLeague(league)
        setCountry(country)
    }

    //funcion livescore para saber si filtra por liga o todas
    let checkMode = (dato) => {
        setLoadAll(dato)
    }

    //funcion lanzada por context para ejecutar modal de detalle de livescore
    let dataModalLiveScore = (dataE,h,a,score) => {
        setLoadLiveScore(false)

        setDataEvent(dataE)
        setDataequipos({h:h,a:a,score:score})
        onOpenModal()
    }

    let onOpenModal = () => {
        setLoadLiveScore(false)
        setOpen(true);
    };

    let onCloseModal = () => {
        setLoadLiveScore(true)
        setOpen(false);
    };

  return (
      <div className="uk-container-largue">
              <Router>
                  <LeagueIdContext.Provider value={{checkData,leagueData,countryData,dataModalLiveScore}}>
                      <Header/>
                      <Selectleague/>
                      <Switch>
                          <Route path="/fixtures" component={() => (
                              <Fixtures/>
                          )}/>
                          <Route path="/livescores" component={() => (
                              <Livescores loadLiveScore={loadLiveScore} loadAll={loadAll} checkMode={checkMode}/>
                          )}/>
                          <Route path="/pastmatches" component={() => (
                              <Pastmatches/>
                          )}/>
                      </Switch>
                      <Modal open={openModal} onClose={() => onCloseModal()} center>
                          {dataEvent.length > 0 && (
                              <Fragment>
                              <h5 className="m-0 pt-0">Marcador {equiposData.score}</h5>
                                  <table className="uk-table uk-table-striped uk-table-responsive">
                                      <thead>
                                      <tr>
                                          <th className="uk-text-secondary">Tiempo</th>
                                          <th className="uk-text-secondary">Jugador</th>
                                          <th className="uk-text-secondary">Equipo</th>
                                          <th className="uk-text-secondary">Evento</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      {Object.keys(dataEvent).map(key => (
                                          <ModalComponent datos={dataEvent[key]} key={key} equipos={equiposData}/>
                                      ))}
                                      </tbody>
                                  </table>
                              </Fragment>
                          )}
                      </Modal>
                  </LeagueIdContext.Provider>
              </Router>
      </div>
  );
}

export default App;
